function konversi() {
    let valas = document.getElementById("uang").value
    let nilai = document.getElementById("rupiah").value
    let hasil
    let konversi = document.getElementById("hasil")
    switch(valas){
        case "us":
            hasil = nilai * 9.915
            konversi.setAttribute("value", hasil)
            break
        case "singapore":
            hasil = nilai * 13.472
            konversi.setAttribute("value", hasil)
            break
        case "malaysia":
            hasil = nilai * 874
            konversi.setAttribute("value", hasil)
            break
        case "jepang":
            hasil = nilai * 120
            konversi.setAttribute("value", hasil)
            break
        case "euro":
            hasil = nilai * 15.888
            konversi.setAttribute("value", hasil)
            break
        case "arab":
            hasil = nilai * 3.592
            konversi.setAttribute("value", hasil)
            break
    }
}